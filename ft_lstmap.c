/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/17 00:09:43 by alangloi          #+#    #+#             */
/*   Updated: 2020/01/07 12:29:33 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, void *(*f)(void *), void (*del)(void *))
{
	t_list	*elem;

	if (!lst || !del)
		return (NULL);
	if (!f)
		return (NULL);
	elem = ft_lstnew(f(lst->content));
	if (!elem)
		return (NULL);
	if (lst->next)
	{
		elem->next = ft_lstmap(lst->next, f, del);
		if (!elem->next)
		{
			ft_lstdelone(elem, del);
			return (NULL);
		}
	}
	else
		elem->next = NULL;
	return (elem);
}
