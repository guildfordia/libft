/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/22 15:06:10 by alangloi          #+#    #+#             */
/*   Updated: 2019/12/30 17:31:14 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	char		*ptr;

	ptr = (0);
	while (*s)
	{
		if (*s == c)
			ptr = (char *)s;
		++s;
	}
	if (ptr)
		return (ptr);
	if (c == '\0')
		return ((char *)s);
	return (0);
}
