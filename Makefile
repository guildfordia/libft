#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/12/11 22:06:08 by alangloi          #+#    #+#              #
#    Updated: 2021/09/08 15:32:51 by alangloi         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME=libft.a

CC=gcc

CFLAGS=-Wall -Wextra -Werror

RM=rm -f

SRC=ft_memset.c ft_bzero.c ft_memcpy.c ft_memccpy.c ft_memmove.c ft_memchr.c ft_memcmp.c ft_strlen.c ft_isdigit.c ft_isalpha.c ft_isalnum.c ft_isascii.c ft_isprint.c ft_toupper.c ft_tolower.c ft_strchr.c ft_strrchr.c ft_strncmp.c ft_strlcpy.c ft_strlcat.c ft_strnstr.c ft_atoi.c ft_calloc.c ft_strdup.c ft_substr.c ft_strjoin.c ft_strtrim.c ft_split.c ft_itoa.c ft_strmapi.c ft_putchar_fd.c ft_putstr_fd.c ft_putendl_fd.c ft_putnbr_fd.c ft_strrev.c ft_memdel.c ft_strncat.c ft_strnew.c ft_memalloc.c ft_strcmp.c ft_strcpy.c ft_strndup.c ft_strncpy.c ft_strnlen.c ft_itoa_base.c ft_intlen.c ft_intlen_u.c ft_putnbr_u_fd.c ft_utoa_base.c ft_sqrt.c ft_atoi_double.c ft_strequ.c ft_strdel.c ft_pow.c ft_realloc.c ft_atof.c

SRC_BONUS=ft_lstnew.c ft_lstadd_front.c ft_lstsize.c ft_lstlast.c ft_lstadd_back.c ft_lstdelone.c ft_lstclear.c ft_lstiter.c ft_lstmap.c

OBJ=$(SRC:.c=.o)

OBJ_BONUS=$(SRC_BONUS:.c=.o)

HEAD=libft.h

all: $(NAME)

$(NAME) : $(OBJ)
	@ar rcs $@ $^

$(OBJ): %.o: %.c $(HEAD)
	@$(CC) $(CFLAGS) -c $< -o $@

bonus: $(OBJ_BONUS) $(OBJ)
	@ar rcs $(NAME) $^

$(OBJ_BONUS): %.o: %.c $(HEAD)
	@$(CC) $(FLAGS) -c $< -o $@

clean:
	@$(RM) $(OBJ) $(OBJ_BONUS)

fclean: clean
	@$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re bonus
