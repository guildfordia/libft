/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/02 13:17:54 by alangloi          #+#    #+#             */
/*   Updated: 2019/12/30 13:36:27 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char	*he_protec(char const *s1, char const *s2)
{
	if (s1 == NULL && s2 == NULL)
		return (ft_strnew(0));
	if (*s1 == '\0')
		return (ft_strdup(s2));
	if (*s2 == '\0')
		return (ft_strdup(s1));
	return (NULL);
}

char	*ft_strjoin(char const *s1, char const *s2)
{
	char		*str;
	size_t		s1_len;
	size_t		s2_len;
	size_t		i;
	size_t		j;

	if (!s1 || !s2)
		return (NULL);
	he_protec(s1, s2);
	s1_len = ft_strlen(s1);
	s2_len = ft_strlen(s2);
	str = ft_strnew(s1_len + s2_len);
	if (str == NULL)
		return (NULL);
	i = -1;
	j = -1;
	while (++i < s1_len)
		str[i] = s1[i];
	while (++j < s2_len)
		str[i++] = s2[j];
	str[i] = '\0';
	return (str);
}
