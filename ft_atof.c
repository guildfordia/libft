/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atof.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <alangloi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/05 17:36:52 by alangloi          #+#    #+#             */
/*   Updated: 2021/09/18 15:27:53 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

double	ft_atof(const char *str)
{
	double	integer;
	double	floating;
	char	*c;
	int		size;

	c = (char *)str;
	integer = (double)ft_atoi(c);
	while (*c && *c != '.')
		c++;
	if (*c == '.')
		c++;
	floating = (double)ft_atoi(c);
	size = ft_strlen(c);
	while (size--)
		floating /= 10;
	if (integer > 0)
		return (integer + floating);
	else
		return (-1 * fabs(integer + floating));
}
