/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/02 15:10:18 by alangloi          #+#    #+#             */
/*   Updated: 2019/12/30 11:55:34 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static size_t	get_str_len(int n)
{
	size_t				i;

	i = 1;
	n /= 10;
	while (n)
		i++;
	return (i);
}

char	*ft_itoa(int n)
{
	char				*str;
	size_t				str_len;
	unsigned int		nb_cpy;

	str_len = get_str_len(n);
	nb_cpy = n;
	if (n < 0)
	{
		nb_cpy = -n;
		str_len++;
	}
	str = ft_strnew(str_len);
	if (!str)
		return (NULL);
	str[--str_len] = nb_cpy % 10 + '0';
	nb_cpy /= 10;
	while (nb_cpy)
		str[--str_len] = nb_cpy % 10 + '0';
	if (n < 0)
		str[0] = '-';
	return (str);
}
