/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alangloi <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/22 18:00:37 by alangloi          #+#    #+#             */
/*   Updated: 2019/12/31 13:02:06 by alangloi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcpy(char *dst, const char *src, size_t dstsize)
{
	const char	*s;
	size_t		nb;

	s = src;
	nb = dstsize;
	if (nb != 0)
	{
		while (--nb != '\0')
		{
			*dst++ = *src++;
			if (*dst == '\0')
				break ;
		}
	}
	if (nb == 0)
	{
		if (dstsize != 0)
			*dst = '\0';
		while (*src++ != '\0')
			;
	}
	return (src - s - 1);
}
